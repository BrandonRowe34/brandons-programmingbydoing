/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmingbydoing;

    import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class ProgrammingByDoing14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner keyboard = new Scanner(System.in);
        
        int age;
        int height1;
        int height2;
        double weight;
        
        System.out.print("How old are you? " );
        age = keyboard.nextInt();
        
        System.out.print("How many feet tall are you? " );
        height1 = keyboard.nextInt();
        
        System.out.print("How many inches tall are you? ");
        height2 = keyboard.nextInt();
        
        
        System.out.print("How much do you weigh? " );
        weight = keyboard.nextDouble();
        
        System.out.println( "So you are " + age + " years old, and you are " + height1 + 
                " feet " + height2 + " inch(es) tall and weigh " + weight + " pounds!");
                
      
        
    }
    
}
